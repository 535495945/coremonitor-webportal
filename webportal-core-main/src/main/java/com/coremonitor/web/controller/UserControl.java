package com.coremonitor.web.controller;

import com.coremonitor.common.vo.TokenVo;
import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.common.response.JsonResponse;
import com.coremonitor.web.config.UserTools;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.service.impl.UserInfoServiceImpl;
import io.swagger.annotations.Api;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName UserControl
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/6 18:03
 * @Version 1.0
 **/
@Api("用户操作管理接口")
@Controller
@RequestMapping("/coremonitor/check")
public class UserControl {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserInfoServiceImpl userInfoService;
    /**
     * 登录接口
     * @param userInfoVo
     * @return
     */
    @RequestMapping(path = "/v1/login/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse loginInfo(@RequestBody UserInfoVo userInfoVo) {
        TokenVo tokenVo = new TokenVo();
        JsonResponse jsonResponse = new JsonResponse<TokenVo>();
        SysUserInfoDo sysUserInfoDo = userInfoService.checkUserEnable(userInfoVo);
        if(userInfoService.checkUserEnable(userInfoVo) != null ){
//            List<String> result = Arrays.asList(sysUserInfoDo.getRole().split(","));
//            List<String> roles = userInfoService.selectUserRoleList(result);
//            String token = UserTools.createMD5();
//            tokenVo.setToken(token);
//            jsonResponse.setStatus(200);
//            jsonResponse.setData(tokenVo);
//            UserTools.userMap.put(token,sysUserInfoDo);

        } else {
            jsonResponse.setStatus(404);
        }
        return jsonResponse;
    }

    /**
     * 注销接口
     * @param userInfoVo
     * @return
     */
    @RequestMapping(path = "/v1/cancellation/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse cancellationInfo(@RequestBody UserInfoVo userInfoVo) {
        logger.info("[maap-webportal-main]ChatbotRegisterInfoController addChatbotRegisterInfo");

        return null;
    }

    /**
     * 获取用户信息
     * @param userInfoVo
     * @return
     */
    @RequestMapping(path = "/v1/userInfo/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse getUserInfo(@RequestBody UserInfoVo userInfoVo) {
        JsonResponse jsonResponse = new JsonResponse<TokenVo>();
        jsonResponse.setStatus(200);
        return null;
    }

    /**
     * 添加用户信息
     * @param userInfoVo
     * @return
     */
    @RequestMapping(path = "/v1/addUserInfo/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse addUserInfo(@RequestBody UserInfoVo userInfoVo) {
        JsonResponse jsonResponse = new JsonResponse<TokenVo>();
        if(UserTools.checkUser(userInfoVo.getToken(),"/coremonitor/check/v1/addUserInfo/requests")){
            jsonResponse.setStatus(200);
        } else {
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }
        return null;
    }
}
