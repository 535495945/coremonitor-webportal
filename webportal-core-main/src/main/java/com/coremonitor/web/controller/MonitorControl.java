package com.coremonitor.web.controller;

import com.alibaba.fastjson.JSON;
import com.coremonitor.common.page.BasePage;
import com.coremonitor.common.request.*;
import com.coremonitor.common.response.JsonResponse;
import com.coremonitor.common.vo.*;
import com.coremonitor.web.config.UserTools;
import com.coremonitor.web.mapper.CoreMonitorMapper;
import com.coremonitor.web.service.CoreMonitorService;
import com.coremonitor.web.service.impl.CoreMonitorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName MonitorControl
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/6 19:53
 * @Version 1.0
 **/
@RestController
@RequestMapping("/coremonitor/check")
public class MonitorControl {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CoreMonitorServiceImpl coreMonitorService;
    /**
     * 实时碳块数据监控接口
     * @param userInfoVo
     * @return
     */
    @RequestMapping(path = "/v1/monitor/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse monitorInfo(@RequestBody UserInfoVo userInfoVo) {
        logger.info("[maap-webportal-main]ChatbotRegisterInfoController addChatbotRegisterInfo");
        JsonResponse jsonResponse = new JsonResponse<MonitorInfoVo>();
        if(UserTools.checkUser(userInfoVo.getToken(),"/coremonitor/check/v1/monitor/requests")){
            MonitorInfoVo monitorInfoVo = coreMonitorService.monitorInfo();
            if(monitorInfoVo == null){
                jsonResponse.setStatus(404);
            } else {
                jsonResponse.setData(monitorInfoVo);
                jsonResponse.setStatus(200);
            }
        } else {
            //权限不足
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }

        return jsonResponse;
    }

    /**
     * 图像查询接口
     * @param imageQuereReq
     * @return
     */
    @RequestMapping(path = "/v1/imgquery/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse imgqueryInfo(@RequestBody ImageQuereReq imageQuereReq) {
        logger.info("imgqueryInfo");
        JsonResponse jsonResponse = new JsonResponse<BasePage<List<ImageQueryInfoVo>>>();
        if(UserTools.checkUser(imageQuereReq.getToken(),"/coremonitor/check/v1/imgquery/requests")){
            BasePage<List<ImageQueryInfoVo>> baseData = new BasePage<List<ImageQueryInfoVo>>();
            List<ImageQueryInfoVo> imageQueryInfoVos = coreMonitorService.imgqueryInfo(imageQuereReq);
            baseData.setData(imageQueryInfoVos);
            baseData.setCurrentPage(imageQueryInfoVos.size());
            baseData.setTotal(coreMonitorService.imgqueryInfoTotal(imageQuereReq));
            baseData.setPageCount(baseData.getTotal()/imageQuereReq.getPageSize() + 1 );
            jsonResponse.setStatus(200);
            jsonResponse.setData(baseData);
        } else{
            //权限不足
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }
        return jsonResponse;
    }

    /**
     * 碳块详情查询接口
     * @param carbonBlockDetailsReq
     * @return
     */
    @RequestMapping(path = "/v1/carbonBlockDetails/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse carbonBlockDetails(@RequestBody CarbonBlockDetailsReq carbonBlockDetailsReq) {
        logger.info("carbonBlockDetails");
        JsonResponse jsonResponse = new JsonResponse<CarbonBlockDetailsVo>();
        if(UserTools.checkUser(carbonBlockDetailsReq.getToken(),"/coremonitor/check/v1/carbonBlockDetails/requests")){
            CarbonBlockDetailsVo carbonBlockDetailsVo = coreMonitorService.carbonBlockDetails(carbonBlockDetailsReq.getCarbonBlockNum());
            if(carbonBlockDetailsVo != null){
                jsonResponse.setData(carbonBlockDetailsVo);
                jsonResponse.setStatus(200);
            } else {
                jsonResponse.setStatus(404);
            }
        } else {
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }
        return jsonResponse;
    }

    /**
     * 碳块删除接口
     * @param delectBlockReq
     * @return
     */
    @RequestMapping(path = "/v1/delect/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse delect(@RequestBody DelectBlockReq delectBlockReq) {
        logger.info("delect");
        JsonResponse jsonResponse = new JsonResponse<>();
        if(UserTools.checkUser(delectBlockReq.getToken(),"/coremonitor/check/v1/delect/requests")){
            if(coreMonitorService.delect(delectBlockReq.getCarbonBlockNum())){
                jsonResponse.setStatus(200);
            } else {
                jsonResponse.setStatus(301);
            }
        } else {
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }
        return jsonResponse;
    }
    /**
     * 已读标记接口
     * @param carbonBlockDetailsReq
     * @return
     */
    @RequestMapping(path = "/v1/sign/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse sign(@RequestBody CarbonBlockDetailsReq carbonBlockDetailsReq) {
        logger.info("/v1/sign/requests " + " sign:" + " carbonBlockDetailsReq:" + JSON.toJSONString(carbonBlockDetailsReq));
        JsonResponse jsonResponse = new JsonResponse<>();
        if(UserTools.checkUser(carbonBlockDetailsReq.getToken(),"/coremonitor/check/v1/sign/requests")){
            if(coreMonitorService.sign(carbonBlockDetailsReq.getCarbonBlockNum())){
                jsonResponse.setStatus(200);
            } else {
                jsonResponse.setStatus(301);
            }
        } else {
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }
        return jsonResponse;
    }

    /**
     * 报表查询接口
     * @param reportFormQueryReq
     * @return
     */
    @RequestMapping(path = "/v1/reportFormQuery/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse reportFormQuery(@RequestBody ReportFormQueryReq reportFormQueryReq) {
        logger.info("reportFormQuery");
        JsonResponse jsonResponse = new JsonResponse<List<ReportFormInfoVo>>();
        if(UserTools.checkUser(reportFormQueryReq.getToken(),"/coremonitor/check/v1/reportFormQuery/requests")){
            List<ReportFormInfoVo> reportFormInfoVoList = coreMonitorService.reportFormQuery(reportFormQueryReq);
            if(reportFormInfoVoList != null){
                jsonResponse.setStatus(200);
                jsonResponse.setData(reportFormInfoVoList);
            } else {
                jsonResponse.setStatus(404);
            }
        } else {
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }

        return jsonResponse;
    }

    /**
     * 统计分析接口
     * @param reportFormQueryReq
     * @return
     */
    @RequestMapping(path = "/v1/statistics/requests", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse statistics(@RequestBody ReportFormQueryReq reportFormQueryReq) {
        logger.info("statistics");
        JsonResponse jsonResponse = new JsonResponse<List<StatisticsInfoVo>>();
        if(UserTools.checkUser(reportFormQueryReq.getToken(),"/coremonitor/check/v1/statistics/requests")){
            List<StatisticsInfoVo> statisticsInfoVoList = new LinkedList<StatisticsInfoVo>();
            for (int i = 0 ; i < 10 ; i++)
            {
                StatisticsInfoVo statisticsInfoVo = new StatisticsInfoVo();
                statisticsInfoVo.setBlockTotal(20);
                statisticsInfoVo.setNormalNum(10);
                statisticsInfoVo.setDefectNum(8);
                statisticsInfoVo.setDefectState(12);
                statisticsInfoVo.setYield("90%");
                statisticsInfoVoList.add(statisticsInfoVo);
            }
            jsonResponse.setStatus(200);
            jsonResponse.setData(statisticsInfoVoList);
        } else {
            jsonResponse.setStatus(401);
            jsonResponse.setReason("权限不足");
        }
        return jsonResponse;
    }
}
