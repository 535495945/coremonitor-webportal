package com.coremonitor.web.controller;

import com.coremonitor.common.response.JsonResponse;
import com.coremonitor.common.response.StatusCode;
import com.coremonitor.web.fastdfs.FastDFSClientWrapper;
import com.coremonitor.web.fastdfs.PicUtils;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName FdfdController
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/8 23:38
 * @Version 1.0
 **/
@RestController
@RequestMapping("/fileservice/fdfs/common")
public class FdfdController {


    @Autowired
    private FastDFSClientWrapper fdfs;

    @Autowired
    private FastFileStorageClient storageClient;

    @Autowired
    private PicUtils picUtils;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 上传文件缩略图(File)
     * 由定时任务轮询,上传到fastdfs
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(path = "/uploadMultipartFileForImage", method = RequestMethod.POST)
    public JsonResponse<String> uploadMultipartFileByImage(MultipartFile file) throws Exception {
        String imgUrl = null;
        try {
            logger.info("upload MultipartFile for image start!");
            imgUrl = fdfs.uploadFile(file);
            picUtils.compressPicForScale(file, (List<Long>) Arrays.asList(10 * 1024L, 500 * 1024L), imgUrl);
        } catch (IOException e) {
            e.printStackTrace();
            return new JsonResponse(StatusCode.SERVER_ERROR.intValue(), "系统异常", e.getMessage());
        }
        return new JsonResponse(StatusCode.SUCCESS.intValue(), "success", imgUrl);
    }

    /**
     * 上传文件缩略图(File)
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(path = "/uploadFileForImage", method = RequestMethod.POST)
    public JsonResponse<String> uploadFileByImage(File file) throws Exception {
        String imgUrl = null;
        try {
            logger.info("upload file for image start!");
            imgUrl = fdfs.uploadFile(file);
            InputStream fileInputStream = new FileInputStream(file);
            String fileExtName = file.getName().substring(file.getName().lastIndexOf(".") + 1);
            picUtils.compressPicForScale(fileInputStream, (List<Long>) Arrays.asList(10 * 1024L, 500 * 1024L), imgUrl, fileExtName);
        } catch (IOException e) {
            e.printStackTrace();
            return new JsonResponse(StatusCode.SERVER_ERROR.intValue(), "系统异常", e.getMessage());
        }
        return new JsonResponse(StatusCode.SUCCESS.intValue(), "success", imgUrl);
    }
}
