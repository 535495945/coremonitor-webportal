package com.coremonitor.web.config;

import com.alibaba.fastjson.JSON;
import com.coremonitor.common.vo.TokenVo;
import com.coremonitor.common.vo.UserInfoVo;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName CustomAuthenticationFilter
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/7 0:29
 * @Version 1.0
 **/
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String param= null;
        if(request.getContentType().equals(MediaType.APPLICATION_JSON_UTF8_VALUE)
                ||request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)){
            UsernamePasswordAuthenticationToken authRequest=null;
            String json =  UserTools.ReadAsChars(request);
            UserInfoVo tokenVo = JSON.parseObject(json,UserInfoVo.class);
            if(tokenVo.getUsername() != null && !tokenVo.getUsername().equals("")){
                authRequest=new UsernamePasswordAuthenticationToken(tokenVo.getToken(),tokenVo.getToken());
            } else {
                authRequest=new UsernamePasswordAuthenticationToken(tokenVo.getToken(),tokenVo.getPassword());
            }

            return this.getAuthenticationManager().authenticate(authRequest);
        } else {
            return super.attemptAuthentication(request, response);
        }

    }
}
