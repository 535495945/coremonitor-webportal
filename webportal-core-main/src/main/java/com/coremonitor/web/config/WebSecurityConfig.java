package com.coremonitor.web.config;

import com.alibaba.fastjson.JSON;
import com.coremonitor.common.response.JsonResponse;
import com.coremonitor.common.vo.MonitorInfoVo;
import com.coremonitor.common.vo.RoleVo;
import com.coremonitor.common.vo.TokenVo;
import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.service.impl.UserInfoServiceImpl;
import com.coremonitor.web.service.impl.UserRoleServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName WebSecurityConfig
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/4 23:24
 * @Version 1.0
 **/
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {

    @Autowired
    UserRoleServiceImpl userRoleService;
    @Autowired
    CustomMetadataSource metadataSource;
    @Autowired
    UrlAccessDecisionManager urlAccessDecisionManager;
    @Autowired
    AuthenticationAccessDeniedHandler deniedHandler;
    @Autowired
    UserInfoServiceImpl userInfoService;
    /**
     * 加载权限配置
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userRoleService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/index.html", "/**", "/login_p", "/favicon.ico");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                        o.setSecurityMetadataSource(metadataSource);
                        o.setAccessDecisionManager(urlAccessDecisionManager);
                        return o;
                    }
                })
                .and()
                .formLogin().loginPage("/coremonitor/check/v1/login/requests").loginProcessingUrl("/coremonitor/check/v1/login/requests")
                .usernameParameter("username").passwordParameter("password")
                .failureHandler(new AuthenticationFailureHandler() {
                    @Override
                    public void onAuthenticationFailure(HttpServletRequest req,
                                                        HttpServletResponse resp,
                                                        AuthenticationException e) throws IOException {
                        String json = UserTools.ReadAsChars(req);
                        UserInfoVo userInfoVo = JSON.parseObject(json,UserInfoVo.class);
                        resp.setContentType("application/json;charset=utf-8");
                        JsonResponse jsonResponse = new JsonResponse<TokenVo>();
                        TokenVo tokenVo = new TokenVo();
                        SysUserInfoDo sysUserInfoDo = userInfoService.checkUserEnable(userInfoVo);
                        if(sysUserInfoDo != null ){
                            List<String> result = Arrays.asList(sysUserInfoDo.getRole().split(","));
                            List<Integer> roleList = new ArrayList<>();
                            List<RoleVo> roleVoList = new ArrayList<>();
                            for(int i = 0 ;i < result.size() ; i ++){
                                roleList.add(Integer.parseInt(result.get(i)));
                                RoleVo roleVo = new RoleVo();
                                roleVo.setUrl(result.get(i));
                                roleVoList.add(roleVo);
                            }
                            List<String> roles = userInfoService.selectUserRoleList(roleList);
                            String token = UserTools.createMD5();
                            tokenVo.setToken(token);
                            jsonResponse.setStatus(200);
                            jsonResponse.setData(tokenVo);
                            userInfoVo.setRoles(roleVoList);
                            UserTools.userMap.put(token,userInfoVo);
                        } else {
                            jsonResponse.setStatus(404);
                        }
                        resp.setStatus(401);
                        ObjectMapper om = new ObjectMapper();
                        PrintWriter out = resp.getWriter();
                        out.write(om.writeValueAsString(jsonResponse));
                        out.flush();
                        out.close();
                    }
                })
                .successHandler(new AuthenticationSuccessHandler() {
                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest req,
                                                        HttpServletResponse resp,
                                                        Authentication auth) throws IOException {
                        resp.setContentType("application/json;charset=utf-8");
                        JsonResponse jsonResponse = new JsonResponse<>();
                        jsonResponse.setStatus(200);
                        ObjectMapper om = new ObjectMapper();
                        PrintWriter out = resp.getWriter();
                        out.write(om.writeValueAsString(jsonResponse));
                        out.flush();
                        out.close();
                    }
                })
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(new LogoutSuccessHandler() {
                    @Override
                    public void onLogoutSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication authentication) throws IOException, ServletException {
                        resp.setContentType("application/json;charset=utf-8");
                        JsonResponse jsonResponse = new JsonResponse<>();
                        jsonResponse.setStatus(200);
                        jsonResponse.setReason("注销成功");
                        ObjectMapper om = new ObjectMapper();
                        PrintWriter out = resp.getWriter();
                        out.write(om.writeValueAsString(jsonResponse));
                        out.flush();
                        out.close();
                    }
                })
                .permitAll()
                .and().csrf().disable()
                .exceptionHandling().accessDeniedHandler(deniedHandler);
    }
}
