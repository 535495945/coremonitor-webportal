package com.coremonitor.web.config;

import com.alibaba.fastjson.JSON;
import com.coremonitor.common.vo.TokenVo;
import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.dao.SysUserRoleInfoDo;
import com.coremonitor.web.service.impl.UserInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.Collection;
import java.util.List;

/**
 * @ClassName CustomMetadataSource
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/4 23:36
 * @Version 1.0
 **/
@Component
public class CustomMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Autowired
    UserInfoServiceImpl userInfoService;

    AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public Collection<ConfigAttribute> getAttributes(Object o) {
        String requestUrl = ((FilterInvocation) o).getRequestUrl();
        List<SysUserRoleInfoDo> sysUserRoleInfoDoList = userInfoService.selectAllMenu();

        if(sysUserRoleInfoDoList != null){

            for(int i = 0 ; i < sysUserRoleInfoDoList.size() ; i++){
                if(antPathMatcher.match(sysUserRoleInfoDoList.get(i).getUrl(),requestUrl)){

                    return SecurityConfig.createList(sysUserRoleInfoDoList.get(i).getId() + "");
                }
            }
        }
        //没有匹配上的资源，都是登录访问
        return SecurityConfig.createList("/coremonitor/check/v1/login/requests");
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }
    @Override
    public boolean supports(Class<?> aClass) {
        return FilterInvocation.class.isAssignableFrom(aClass);
    }
}
