package com.coremonitor.web.config;

import com.coremonitor.common.response.JsonResponse;
import com.coremonitor.common.vo.TokenVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ClassName AuthenticationAccessDeniedHandler
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/4 23:13
 * @Version 1.0
 **/
@Component
public class AuthenticationAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse resp,
                       AccessDeniedException e) throws IOException {
        resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        JsonResponse jsonResponse = new JsonResponse<TokenVo>();
        jsonResponse.setStatus(401);
        jsonResponse.setReason("权限不足,请联系管理员");
        out.write(new ObjectMapper().writeValueAsString(jsonResponse));
        out.flush();
        out.close();
    }
}
