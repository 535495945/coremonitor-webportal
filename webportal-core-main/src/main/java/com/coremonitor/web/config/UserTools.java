package com.coremonitor.web.config;

import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.dao.SysUserInfoDo;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.AntPathMatcher;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName UserTools
 * @Description 全局用户数据存储
 * @Author ROS
 * @Date 2019/5/5 20:51
 * @Version 1.0
 **/
public class UserTools
{
    public static ConcurrentHashMap<String,UserInfoVo> userMap = new ConcurrentHashMap<>();
    private static AntPathMatcher antPathMatcher = new AntPathMatcher();
    /**
     * 随机生成一个Md5密钥
     * @return
     */
    public static String createMD5(){
        return DigestUtils.md5Hex(UUID.randomUUID().toString());
    }

    // 字符串读取
    // 方法一
    public static String ReadAsChars(HttpServletRequest request)
    {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder("");
        try
        {
            br = request.getReader();
            String str;
            while ((str = br.readLine()) != null)
            {
                sb.append(str);
            }
            br.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (null != br)
            {
                try
                {
                    br.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public static boolean checkUser(String token,String url){
        UserInfoVo userInfoVo = userMap.get(token);
        if(userInfoVo != null){
            for(int i = 0;i < userInfoVo.getRoles().size() ; i++){
                if(antPathMatcher.match(userInfoVo.getRoles().get(i).getUrl(),url)){
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }
}
