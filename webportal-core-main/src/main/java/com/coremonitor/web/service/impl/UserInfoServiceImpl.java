package com.coremonitor.web.service.impl;

import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.dao.SysUserRoleInfoDo;
import com.coremonitor.web.mapper.UserInfoMapper;
import com.coremonitor.web.service.UserInfoService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName UserInfoServiceImpl
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/20 11:15
 * @Version 1.0
 **/
@Service(value = "UserInfoService")
@Transactional
public class UserInfoServiceImpl implements UserInfoService {

    @Resource
    UserInfoMapper userInfoMapper;

    @Override
    public SysUserInfoDo checkUserEnable(UserInfoVo userInfoVo) {
        try{
            SysUserInfoDo userInfo = userInfoMapper.selectByUserNmae(userInfoVo.getUsername());
            if(userInfo != null && userInfo.getPassword().equals(DigestUtils.md5Hex(userInfoVo.getPassword()))){
                return userInfo;
            }
        } catch (Exception ex )
        {
            ex.getMessage();
            return  null;
        }
        return null;
    }

    @Override
    public List<String> selectUserRoleList(List<Integer> roleId) {
        return userInfoMapper.selectUserRoleList(roleId);
    }

    @Override
    public List<SysUserRoleInfoDo> selectAllMenu() {
        return userInfoMapper.selectAllMenu();
    }
}
