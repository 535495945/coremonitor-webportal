package com.coremonitor.web.service;

import com.coremonitor.common.request.ImageQuereReq;
import com.coremonitor.common.request.ReportFormQueryReq;
import com.coremonitor.common.vo.*;

import java.util.List;

/**
 * @ClassName CoreMonitorService
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/20 16:38
 * @Version 1.0
 **/
public interface CoreMonitorService {

    /**
     * 实时碳块数据监控接口
     * @return
     */
    public MonitorInfoVo monitorInfo();

    /**
     * 图像查询接口
     * @return
     */
    public List<ImageQueryInfoVo> imgqueryInfo(ImageQuereReq imgqueryInDo);

    /**
     * 查询图像总数
     * @param imgqueryInDo
     * @return
     */
    public int imgqueryInfoTotal(ImageQuereReq imgqueryInDo);
    /**
     * 碳块详情
     * @return
     */
    public CarbonBlockDetailsVo carbonBlockDetails(String num);

    /**
     * 删除碳块
     * @return
     */
    public boolean delect(String num);

    /**
     * 标记碳块
     * @return
     */
    public boolean sign(String num);

    /**
     * 报表查询
     * @return
     */
    public List<ReportFormInfoVo> reportFormQuery(ReportFormQueryReq reportFormQueryReq);

    /**
     * 统计
     * @return
     */
    public List<StatisticsInfoVo> statistics(ReportFormQueryReq reportFormQueryReq);
}
