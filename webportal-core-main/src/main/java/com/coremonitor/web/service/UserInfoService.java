package com.coremonitor.web.service;

import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.dao.SysUserRoleInfoDo;

import java.util.List;

/**
 * @ClassName UserInfoService
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/20 11:14
 * @Version 1.0
 **/
public interface UserInfoService {
    /**
     * 判断用户是否可用
     * @param userInfoVo
     * @return
     */
    SysUserInfoDo checkUserEnable(UserInfoVo userInfoVo);

    List<String> selectUserRoleList(List<Integer> roleId);
    List<SysUserRoleInfoDo> selectAllMenu();
}
