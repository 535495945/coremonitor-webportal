package com.coremonitor.web.service.impl;

import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.config.UserTools;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.mapper.UserInfoMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName UserRoleServiceImpl
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/4 22:58
 * @Version 1.0
 **/
@Service
@Transactional
public class UserRoleServiceImpl implements UserDetailsService {

    @Resource
    UserInfoMapper userInfoMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserInfoVo userInfoVo = new UserInfoVo();
        userInfoVo.setToken(username);
//        SysUserInfoDo sysUserInfoDo = UserTools.userMap.get(userInfoVo.getToken());
//        if(sysUserInfoDo == null ){
//            throw  new UsernameNotFoundException("用户名不对");
//        }
//        List<String> roles = userInfoMapper.selectUserRoleList(Arrays.asList(sysUserInfoDo.getRole().split(",")));

        return userInfoVo;
    }
}
