package com.coremonitor.web.service.impl;

import com.coremonitor.common.request.ImageQuereReq;
import com.coremonitor.common.request.ReportFormQueryReq;
import com.coremonitor.common.response.JsonResponse;
import com.coremonitor.common.vo.*;
import com.coremonitor.web.dao.CarbonBlockPhotoInfoDo;
import com.coremonitor.web.dao.ReportQueryDo;
import com.coremonitor.web.dao.SysCarbonBlockInfoDo;
import com.coremonitor.web.mapper.CoreMonitorMapper;
import com.coremonitor.web.mapper.UserInfoMapper;
import com.coremonitor.web.service.CoreMonitorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CoreMonitorServiceImpl
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/20 16:39
 * @Version 1.0
 **/
@Service(value = "CoreMonitorService")
@Transactional
public class CoreMonitorServiceImpl implements CoreMonitorService {

    @Resource
    CoreMonitorMapper coreMonitorMapper;

    @Override
    public MonitorInfoVo monitorInfo() {

        SysCarbonBlockInfoDo sysCarbonBlockInfoDo = coreMonitorMapper.selectLastCarboBlock();

        MonitorInfoVo monitorInfoVo = new MonitorInfoVo();
        monitorInfoVo.setCarbonBlockHandle(coreMonitorMapper.selectCarbonBlockHandle(sysCarbonBlockInfoDo.getNumber()));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        monitorInfoVo.setCreateTime(formatter.format(sysCarbonBlockInfoDo.getCtime()));
        monitorInfoVo.setCarbonBlockNum(sysCarbonBlockInfoDo.getNumber());
        monitorInfoVo.setDefectDetails(sysCarbonBlockInfoDo.getDefect_details());
        monitorInfoVo.setDefectNum(coreMonitorMapper.selectAllDefectNum());
        monitorInfoVo.setLineNum(sysCarbonBlockInfoDo.getProlineid());
        monitorInfoVo.setSignStatus(sysCarbonBlockInfoDo.getSpurtcode());
        monitorInfoVo.setStatus(sysCarbonBlockInfoDo.getState());
        monitorInfoVo.setTheDayNum(coreMonitorMapper.selectTodayNum());
        List<CarbonBlockPhotoInfoDo> carbonBlockPhotoInfoDos = coreMonitorMapper.selectPhotoInfoListByNum(sysCarbonBlockInfoDo.getNumber());
        List<PhotoInfoVo> photoInfoVos = new LinkedList<PhotoInfoVo>();
        for( int i = 0 ; i < carbonBlockPhotoInfoDos.size() ; i++) {
            PhotoInfoVo photoInfoVo = new PhotoInfoVo();
            photoInfoVo.setPhotoState(carbonBlockPhotoInfoDos.get(i).getState());
            photoInfoVo.setPhotoName(carbonBlockPhotoInfoDos.get(i).getPhotonum());
            photoInfoVo.setPhotoPath(carbonBlockPhotoInfoDos.get(i).getOriginalpath());
            photoInfoVo.setResultInfo(carbonBlockPhotoInfoDos.get(i).getOriginalinfo());
            photoInfoVos.add(photoInfoVo);
        }
        monitorInfoVo.setPhotoInfoVo(photoInfoVos);
        return monitorInfoVo;
    }

    @Override
    public List<ImageQueryInfoVo> imgqueryInfo(ImageQuereReq imgqueryInDo) {
        List<ImageQueryInfoVo> imageQueryInfoVos = new LinkedList<ImageQueryInfoVo>();
        if(imgqueryInDo.getFirstIndex() == null){
            imgqueryInDo.setFirstIndex(0);
        }
        if(imgqueryInDo.getPageSize() == null){
            imgqueryInDo.setPageSize(20);
        }

        List<SysCarbonBlockInfoDo> sysCarbonBlockInfoDos = coreMonitorMapper.selectImgqueryInfo(imgqueryInDo);
        for(int i = 0; i < sysCarbonBlockInfoDos.size();i++){
            ImageQueryInfoVo imageQueryInfoVo = new ImageQueryInfoVo();
            imageQueryInfoVo.setCarbonBlockNum(sysCarbonBlockInfoDos.get(i).getNumber());
            imageQueryInfoVo.setStatus(sysCarbonBlockInfoDos.get(i).getState());
            imageQueryInfoVo.setReadState(sysCarbonBlockInfoDos.get(i).getReadstate());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            imageQueryInfoVo.setCreateTime(formatter.format(sysCarbonBlockInfoDos.get(i).getCtime()));
            imageQueryInfoVos.add(imageQueryInfoVo);
        }
        return imageQueryInfoVos;
    }


    @Override
    public int  imgqueryInfoTotal(ImageQuereReq imgqueryInDo) {
        return coreMonitorMapper.selectImgqueryInfoCount(imgqueryInDo);
    }

    @Override
    public CarbonBlockDetailsVo carbonBlockDetails(String num){
        CarbonBlockDetailsVo carbonBlockDetailsVo = new CarbonBlockDetailsVo();
        SysCarbonBlockInfoDo sysCarbonBlockInfoDo = coreMonitorMapper.selectCarbonBlockByNum(num);
        carbonBlockDetailsVo.setCarbonBlockNum(sysCarbonBlockInfoDo.getNumber());
        carbonBlockDetailsVo.setSignStatus(sysCarbonBlockInfoDo.getSpurtcode());
        carbonBlockDetailsVo.setStatus(sysCarbonBlockInfoDo.getState());
        carbonBlockDetailsVo.setLineNum(sysCarbonBlockInfoDo.getProlineid());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        carbonBlockDetailsVo.setCreateTime(formatter.format(sysCarbonBlockInfoDo.getCtime()));
        carbonBlockDetailsVo.setDefectDetails(sysCarbonBlockInfoDo.getDefect_details());
        List<CarbonBlockPhotoInfoDo> carbonBlockPhotoInfoDos = coreMonitorMapper.selectPhotoInfoListByNum(num);
        List<PhotoInfoVo> photoInfoVos = new LinkedList<PhotoInfoVo>();
        for( int i = 0 ; i < carbonBlockPhotoInfoDos.size() ; i++) {
            PhotoInfoVo photoInfoVo = new PhotoInfoVo();
            photoInfoVo.setPhotoState(carbonBlockPhotoInfoDos.get(i).getState());
            photoInfoVo.setPhotoName(carbonBlockPhotoInfoDos.get(i).getPhotonum());
            photoInfoVo.setPhotoPath(carbonBlockPhotoInfoDos.get(i).getOriginalpath());
            photoInfoVo.setResultInfo(carbonBlockPhotoInfoDos.get(i).getOriginalinfo());
            photoInfoVos.add(photoInfoVo);
        }
        carbonBlockDetailsVo.setPhotoInfoVoList(photoInfoVos);
        return carbonBlockDetailsVo;
    }

    @Override
    public boolean delect(String num) {
        try{
            coreMonitorMapper.deleteCarbonBlock(num);
            return true;
        } catch (Exception ex){
            ex.getMessage();
            return false;
        }
    }

    @Override
    public boolean sign(String num) {
        try{
            coreMonitorMapper.sign(num);
            return true;
        }catch (Exception ex){
            ex.getMessage();
            return false;
        }
    }

    @Override
    public List<ReportFormInfoVo> reportFormQuery(ReportFormQueryReq reportFormQueryReq) {
        List<ReportQueryDo> reportQueryDoList = new LinkedList<ReportQueryDo>();
        List<ReportFormInfoVo> reportFormInfoVoList = new LinkedList<ReportFormInfoVo>();
        Map<String ,ReportFormInfoVo> reportFormQueryReqMap = new LinkedHashMap<>();
        if(reportFormQueryReq.getDefectState() == 1){
            //按月
            reportQueryDoList = coreMonitorMapper.selectMonthsReportQuery(reportFormQueryReq);
        } else {
            reportQueryDoList = coreMonitorMapper.selectDayReportQuery(reportFormQueryReq);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(reportFormQueryReq.getCheckType() == 1){
            //缺陷类型
            for(int i = 0 ; i < reportQueryDoList.size() ; i++){
               String timeSign = formatter.format(reportQueryDoList.get(i).getDays());
                ReportFormInfoVo req = reportFormQueryReqMap.get(timeSign);
               if(req == null){
                   req = new ReportFormInfoVo();
                   if(reportQueryDoList.get(i).getState() == 0){
                        req.setDefectNum(reportQueryDoList.get(i).getCount());
                   } else if(reportQueryDoList.get(i).getState() == 1){
                       req.setNormalNum(reportQueryDoList.get(i).getCount());
                   }
                   req.setTime(timeSign);
                   reportFormQueryReqMap.put(timeSign,req);
               } else {
                   if(reportQueryDoList.get(i).getState() == 0){
                       req.setBlockTotal(req.getNormalNum() + reportQueryDoList.get(i).getCount());
                   } else if(reportQueryDoList.get(i).getState() == 1){
                       req.setBlockTotal(req.getDefectNum() + reportQueryDoList.get(i).getCount());
                   }
               }
            }

            for(Map.Entry<String,ReportFormInfoVo> entry:reportFormQueryReqMap.entrySet()){
                System.out.println(entry.getKey()+" "+entry.getValue());
                ReportFormInfoVo reportFormInfo = new ReportFormInfoVo();
                reportFormInfo.setTime(entry.getValue().getTime());
                reportFormInfo.setBlockTotal(entry.getValue().getBlockTotal());
                reportFormInfo.setNormalNum(entry.getValue().getNormalNum());
                reportFormInfo.setDefectNum(entry.getValue().getDefectNum());
                reportFormInfo.setYield(entry.getValue().getNormalNum()/entry.getValue().getBlockTotal() * 100 + "%");
                reportFormInfoVoList.add(reportFormInfo);
            }
        } else {

        }

        return reportFormInfoVoList;
    }

    @Override
    public List<StatisticsInfoVo> statistics(ReportFormQueryReq reportFormQueryReq) {
        List<ReportQueryDo> reportQueryDoList = new LinkedList<ReportQueryDo>();
        reportQueryDoList = coreMonitorMapper.selectDayReportQuery(reportFormQueryReq);
        List<StatisticsInfoVo> statisticsInfoVoList = new LinkedList<StatisticsInfoVo>();
        Map<String ,StatisticsInfoVo> statisticsInfoVoMap = new LinkedHashMap<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for(int i = 0 ; i < reportQueryDoList.size() ; i++){
            String timeSign = formatter.format(reportQueryDoList.get(i).getDays());
            StatisticsInfoVo req = statisticsInfoVoMap.get(timeSign);
            if(req == null){
                req = new StatisticsInfoVo();
                if(reportQueryDoList.get(i).getState() == 0){
                    req.setDefectNum(reportQueryDoList.get(i).getCount());
                } else if(reportQueryDoList.get(i).getState() == 1){
                    req.setNormalNum(reportQueryDoList.get(i).getCount());
                }
                req.setTime(timeSign);
                statisticsInfoVoMap.put(timeSign,req);
            } else {
                if(reportQueryDoList.get(i).getState() == 0){
                    req.setBlockTotal(req.getNormalNum() + reportQueryDoList.get(i).getCount());
                } else if(reportQueryDoList.get(i).getState() == 1){
                    req.setBlockTotal(req.getDefectNum() + reportQueryDoList.get(i).getCount());
                }
            }
        }

        for(Map.Entry<String,StatisticsInfoVo> entry:statisticsInfoVoMap.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
            StatisticsInfoVo reportFormInfo = new StatisticsInfoVo();
            reportFormInfo.setTime(entry.getValue().getTime());
            reportFormInfo.setBlockTotal(entry.getValue().getBlockTotal());
            reportFormInfo.setNormalNum(entry.getValue().getNormalNum());
            reportFormInfo.setDefectNum(entry.getValue().getDefectNum());
            reportFormInfo.setYield(entry.getValue().getNormalNum()/entry.getValue().getBlockTotal() * 100 + "%");
            statisticsInfoVoList.add(reportFormInfo);
        }

        return statisticsInfoVoList;
    }
}
