package com.coremonitor.web.mapper;

import com.coremonitor.common.request.ImageQuereReq;
import com.coremonitor.common.request.ReportFormQueryReq;
import com.coremonitor.common.vo.CarbonBlockDetailsVo;
import com.coremonitor.common.vo.ImageQueryInfoVo;
import com.coremonitor.common.vo.PhotoInfoVo;
import com.coremonitor.web.dao.CarbonBlockPhotoInfoDo;
import com.coremonitor.web.dao.ReportQueryDo;
import com.coremonitor.web.dao.SysCarbonBlockInfoDo;

import java.util.List;

public interface CoreMonitorMapper {
    /**
     * 查询当天生产碳块数量
     * @return
     */
    int selectTodayNum();

    /**
     * 查询所有缺陷碳块总数
     * @return
     */
    int selectAllDefectNum();

    /**
     * 查询最近碳块信息
     * @return
     */
    SysCarbonBlockInfoDo selectLastCarboBlock();

    /**
     * 根据碳块编号,查询碳块详情
     */
    SysCarbonBlockInfoDo selectCarbonBlockByNum(String num);

    /**
     * 当前已经识别快照数
     */
    int selectCarbonBlockHandle(String num);

    /**
     * 根据碳块编号查询对应碳块照片消息
     * @param num
     * @return
     */
    List<CarbonBlockPhotoInfoDo> selectPhotoInfoListByNum(String num);

    /**
     * 查询图像信息
     * @return
     */
    List<SysCarbonBlockInfoDo> selectImgqueryInfo(ImageQuereReq imgqueryInDo);

    /**
     * 查询图像信息总数
     * @return
     */
    int selectImgqueryInfoCount(ImageQuereReq imgqueryInDo);

    /**
     * 删除碳块
     */
    void deleteCarbonBlock(String num);

    /**
     * 标记碳块
     * @param num
     */
    void sign(String num);

    /**
     * 按天查询
     * @param reportFormQueryReq
     * @return
     */
    List<ReportQueryDo> selectDayReportQuery(ReportFormQueryReq reportFormQueryReq);

    /**
     * 按月查询
     * @param reportFormQueryReq
     * @return
     */
    List<ReportQueryDo> selectMonthsReportQuery(ReportFormQueryReq reportFormQueryReq);
}
