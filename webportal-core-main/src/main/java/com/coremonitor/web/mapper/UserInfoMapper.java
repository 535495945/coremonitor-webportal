package com.coremonitor.web.mapper;

import com.coremonitor.common.vo.UserInfoVo;
import com.coremonitor.web.dao.SysUserInfoDo;
import com.coremonitor.web.dao.SysUserRoleInfoDo;
import io.swagger.models.auth.In;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @ClassName UserInfoMapper
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/17 22:50
 * @Version 1.0
 **/
@Mapper
public interface UserInfoMapper {

    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    SysUserInfoDo selectByUserNmae(String username);

    List<String> selectUserRoleList(List<Integer> roleId);

    List<SysUserRoleInfoDo> selectAllMenu();
}
