package com.coremonitor.web;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;


import java.util.concurrent.Executor;

/**
 * @ClassName WebPortalBootstrap
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/6 17:53
 * @Version 1.0
 **/
@Import(FdfsClientConfig.class)
@SpringBootApplication
@MapperScan("com.coremonitor.web.mapper")
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)  //注解是为了解决JMX重复注册bean的问题
public class WebPortalBootstrap {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebPortalBootstrap.class, args);
    }

}
