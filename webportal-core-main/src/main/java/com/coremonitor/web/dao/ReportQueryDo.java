package com.coremonitor.web.dao;

import java.util.Date;

/**
 * @ClassName ReportQueryDo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/20 20:10
 * @Version 1.0
 **/
public class ReportQueryDo {
    private Integer state;
    private Date days;
    private Integer count;


    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getDays() {
        return days;
    }

    public void setDays(Date days) {
        this.days = days;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
