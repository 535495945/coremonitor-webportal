package com.coremonitor.web.dao;

import java.util.Date;

/**
 * @ClassName SysCarbonBlockInfoDo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/19 23:21
 * @Version 1.0
 **/
public class SysCarbonBlockInfoDo {
    private Integer id;
    /**
     * 用户id
     */
    private Integer userid;
    /**
     * 生产线编号
     */
    private String prolineid;
    /**
     * 炭块编号
     */
    private String number;
    /**
     * 照片数量
     */
    private Integer photonum;
    /**
     * 当前碳块状态 1.正常 0.缺陷
     */
    private Integer state;
    /**
     * 缺陷类型0 正常1.裂纹2.氧化4.缺角8.跨面裂纹 ，二进制表示
     */
    private Integer type;
    /**
     * 缺陷详情
     */
    private String defect_details;
    /**
     * 碳块操作标记: 0  未操作 1 已处理  2正在处理  3 待处理 4 无需处理，放行
     */
    private Integer operation;
    /**
     * 碳块查询状态，标记已读未读 0 未读  1 已读
     */
    private Integer readstate;
    /**
     * 喷码标记位 0未标记  1 已标记
     */
    private Integer spurtcode;
    /**
     * 更新时间
     */
    private Date utime;
    /**
     * 创建时间
     */
    private Date ctime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getProlineid() {
        return prolineid;
    }

    public void setProlineid(String prolineid) {
        this.prolineid = prolineid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPhotonum() {
        return photonum;
    }

    public void setPhotonum(Integer photonum) {
        this.photonum = photonum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDefect_details() {
        return defect_details;
    }

    public void setDefect_details(String defect_details) {
        this.defect_details = defect_details;
    }

    public Integer getOperation() {
        return operation;
    }

    public void setOperation(Integer operation) {
        this.operation = operation;
    }

    public Integer getReadstate() {
        return readstate;
    }

    public void setReadstate(Integer readstate) {
        this.readstate = readstate;
    }

    public Integer getSpurtcode() {
        return spurtcode;
    }

    public void setSpurtcode(Integer spurtcode) {
        this.spurtcode = spurtcode;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
}
