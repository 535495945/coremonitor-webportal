package com.coremonitor.web.dao;

import java.util.Date;

/**
 * @ClassName CarbonBlockPhotoInfoDo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/19 22:58
 * @Version 1.0
 **/
public class CarbonBlockPhotoInfoDo {
    private Integer id;
    /**
     * 碳块id
     */
    private String carbon_block_id;
    /**
     * 照片编号
     */
    private String photonum;
    /**
     * 相机编号
     */
    private String cameranum;
    /**
     * 照片原始位置
     */
    private String originalpath;
    /**
     * 缺陷的位置坐标
     */
    private String originalinfo;
    /**
     * 碳块缺陷标记位置
     */
    private String defectpath;
    /**
     * 是否有缺陷 1.正常 0.缺陷 2.未处理
     */
    private Integer state;
    /**
     * 缺陷描述
     */
    private String describe;
    /**
     * 缺陷类型0无缺陷1.裂纹2.氧化4.缺角8.跨面裂纹 ，二进制表示
     */
    private Integer type;
    /**
     * 更新时间
     */
    private Date utime;
    /**
     * 创建时间
     */
    private Date ctime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCarbon_block_id() {
        return carbon_block_id;
    }

    public void setCarbon_block_id(String carbon_block_id) {
        this.carbon_block_id = carbon_block_id;
    }

    public String getPhotonum() {
        return photonum;
    }

    public void setPhotonum(String photonum) {
        this.photonum = photonum;
    }

    public String getCameranum() {
        return cameranum;
    }

    public void setCameranum(String cameranum) {
        this.cameranum = cameranum;
    }

    public String getOriginalpath() {
        return originalpath;
    }

    public void setOriginalpath(String originalpath) {
        this.originalpath = originalpath;
    }

    public String getOriginalinfo() {
        return originalinfo;
    }

    public void setOriginalinfo(String originalinfo) {
        this.originalinfo = originalinfo;
    }

    public String getDefectpath() {
        return defectpath;
    }

    public void setDefectpath(String defectpath) {
        this.defectpath = defectpath;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
}
