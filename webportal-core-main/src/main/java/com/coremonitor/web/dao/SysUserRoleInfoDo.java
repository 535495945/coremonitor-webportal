package com.coremonitor.web.dao;

/**
 * @ClassName SysUserRoleInfoDo
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/6 23:57
 * @Version 1.0
 **/
public class SysUserRoleInfoDo {
    private Integer id;
    private String name;
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
