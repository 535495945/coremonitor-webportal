package com.coremonitor.web.fastdfs;

import com.github.tobato.fastdfs.domain.MateData;
import com.github.tobato.fastdfs.domain.StorageNode;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.proto.storage.StorageSetMetadataCommand;
import com.github.tobato.fastdfs.proto.storage.StorageUploadFileCommand;
import com.github.tobato.fastdfs.proto.storage.enums.StorageMetdataSetType;
import com.github.tobato.fastdfs.service.DefaultGenerateStorageClient;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @ClassName PicUtils
 * @Description TODO
 * @Author ROS
 * @Date 2019/5/8 23:48
 * @Version 1.0
 **/
@Component
public class PicUtils  extends DefaultGenerateStorageClient {

    @Value("${fdfs.access.url}")
    private String address;

    @Autowired
    private WebFastFileStorageClient fileStorageClient;

    private static Logger logger = LoggerFactory.getLogger(PicUtils.class);

    /**
     * 1.urlPath 不为null时，在原有地址上拼接后缀上传
     * 2.为null，以最大分辨率为基础地址，desFileSizes后序在此基础上拼接后缀上传
     * 3.desFileSizes 分辨率列表
     * 4.masterFilePath 传递getFullPath !!1
     *
     * @param input
     * @param desFileSizes
     * @param fileExtName
     * @param masterFilePath
     * @return
     * @throws IOException
     */
    public String compressPicForScale(InputStream input, List<Long> desFileSizes, String masterFilePath, String fileExtName) throws IOException {
        byte[] imageBytes = read(input);
        return compressPicForScaleIn(imageBytes, desFileSizes, masterFilePath, fileExtName);
    }

    /**
     * 根据指定大小压缩图片
     * <p>
     * MultipartFile file 上传文件
     * String masterFilePath 原图片上传后的文件路径
     * desFileSize 压缩后大小 KB
     *
     * @return 压缩质量后的图片字节数组
     */
    public String compressPicForScale(MultipartFile file, List<Long> desFileSize, String masterFilePath) throws Exception {
        InputStream inputStream = file.getInputStream();
        String fileExtName = FilenameUtils.getExtension(file.getOriginalFilename());
        String storePath = compressPicForScale(inputStream, desFileSize, masterFilePath, fileExtName);
        return storePath;
    }


    /**
     * 根据指定大小压缩图片
     * <p>
     * byte[] imageBytes 文件bytes
     * String masterFilePath 原图片上传后的文件路径
     * desFileSize 压缩后大小 KB
     *
     * @return 压缩质量后的图片字节数组
     */
    public String compressPicForScaleIn(byte[] imageBytes, List<Long> desFileSizes, String masterFilePath, String fileExtName){
        int size = desFileSizes.size();
        if (size == 0) {
            return null;
        }
        //对压缩规模排序
        Collections.sort(desFileSizes);
        logger.info("【图片压缩】imageId| 图片原大小={}kb ", imageBytes.length / 1024);
        double accuracy = 0;
        try {
            ByteArrayInputStream input;
            while (imageBytes.length > desFileSizes.get(0) && size > 0) {
                accuracy = getAccuracy(imageBytes.length);
                ByteArrayInputStream inputinputStream = new ByteArrayInputStream(imageBytes);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream(imageBytes.length);
                Thumbnails.of(inputinputStream)
                        .scale(accuracy)
                        .toOutputStream(outputStream);
                imageBytes = outputStream.toByteArray();
                if (desFileSizes.get(size - 1) > imageBytes.length) {
                    StorageNode client = trackerClient.getStoreStorage();
                    input = new ByteArrayInputStream(imageBytes);
                    if (masterFilePath != null && masterFilePath != "") {
                        fileStorageClient.uploadStorePath(client,super.connectionManager,masterFilePath, imageBytes.length, fileExtName, outputStream);
                    } else {
                        masterFilePath = uploadFileAndMateData(client, input, imageBytes.length, fileExtName, null).getFullPath();
                        logger.info("【图片压缩】上传成功，path:{},size:{} ", masterFilePath, imageBytes.length / 1024);
                    }
                    size--;
                }
            }
        } catch (Exception e) {
            logger.error("【图片压缩】msg=图片压缩失败!", e);
            throw new RuntimeException("【图片压缩】msg=图片压缩失败!" + e);
        }
        return masterFilePath;
    }



    /**
     * 上传普通文件
     *
     * @param client
     * @param inputStream
     * @param fileSize
     * @param fileExtName
     * @param metaDataSet
     * @return
     */
    private StorePath uploadFileAndMateData(StorageNode client, InputStream inputStream, long fileSize,
                                            String fileExtName, Set<MateData> metaDataSet) {
        // 上传文件
        StorageUploadFileCommand command = new StorageUploadFileCommand(client.getStoreIndex(), inputStream,
                fileExtName, fileSize, false);
        StorePath path = connectionManager.executeFdfsCmd(client.getInetSocketAddress(), command);
        // 上传matadata
        if (hasMateData(metaDataSet)) {
            StorageSetMetadataCommand setMDCommand = new StorageSetMetadataCommand(path.getGroup(), path.getPath(),
                    metaDataSet, StorageMetdataSetType.STORAGE_SET_METADATA_FLAG_OVERWRITE);
            connectionManager.executeFdfsCmd(client.getInetSocketAddress(), setMDCommand);
        }
        return path;
    }

    /**
     * 检查是否有MateData
     *
     * @param metaDataSet
     * @return
     */
    private boolean hasMateData(Set<MateData> metaDataSet) {
        return null != metaDataSet && !metaDataSet.isEmpty();
    }


    /**
     * 自动调节精度(经验数值)
     *
     * @param size 图片现大小
     * @return 图片压缩质量比
     */
    private double getAccuracy(long size) {
        double accuracy;
        if (size < 100 * 1024) {
            accuracy = 0.8;
        } else if (size < 200 * 1024) {
            accuracy = 0.5;
        } else if (size < 400 * 1024) {
            accuracy = 0.44;
        } else {
            accuracy = 0.4;
        }
        return accuracy;
    }

    private static byte[] read(InputStream inputStream) throws IOException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int num = inputStream.read(buffer);
            while (num != -1) {
                baos.write(buffer, 0, num);
                num = inputStream.read(buffer);
            }
            baos.flush();
            return baos.toByteArray();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }


}
