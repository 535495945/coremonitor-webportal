package com.coremonitor.web.fastdfs;

import com.github.tobato.fastdfs.conn.ConnectionManager;
import com.github.tobato.fastdfs.domain.StorageNode;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.domain.ThumbImageConfig;
import com.github.tobato.fastdfs.proto.storage.StorageUploadSlaveFileCommand;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by zengjianfeng.
 *
 * @Date: 2019/4/1 5:05 PM
 * <p>
 * Copyright (c) 2011 北京新媒传信科技有限公司
 */
@Component
public class WebFastFileStorageClient {

    private static Logger logger = LoggerFactory.getLogger(PicUtils.class);

    /** 缩略图生成配置 */
    @Resource
    private ThumbImageConfig thumbImageConfig;
    /**
     * 在原有路径上传文件(上传已压缩的文件)
     *
     * @param masterFilePath        原有路径
     * @param desFileSize           文件大小
     * @param fileExtName           文件后缀名 如：png
     * @param byteArrayOutputStream 文件流
     * @return
     */
    @Async
    public StorePath uploadStorePath(StorageNode client, ConnectionManager connectionManager,String masterFilePath, long desFileSize, String fileExtName, ByteArrayOutputStream byteArrayOutputStream){
        String prefixName = getImgSuffix(desFileSize);
        ByteArrayInputStream thumbImageStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        // 获取文件大小
        long fileSize = thumbImageStream.available();
        // 获取缩略图前缀
        masterFilePath = StringUtils.substringAfter(masterFilePath, "/");
        StorageUploadSlaveFileCommand command = new StorageUploadSlaveFileCommand(thumbImageStream, fileSize,
                masterFilePath, prefixName, fileExtName);
        StorePath storePath = new StorePath();
        try {
            //上传间隔 要依据已上传的封面进行压缩 所以会延迟 找不到节点或文件
            Thread.sleep(10000);
            storePath = connectionManager.executeFdfsCmd(client.getInetSocketAddress(), command);
        }catch (Exception e){
            logger.error("上传压缩文件失败：{}",e.getMessage());
            throw new RuntimeException("上传压缩文件失败!");
        }
        logger.info("【图片压缩】imageId| 压缩后大小={}kb,path:{}"
                , fileSize / 1024, storePath.getFullPath());
        return storePath;
    }

    private static String getImgSuffix(long desFileSize) {
        if (10 * 1024 > desFileSize) {
            return "_small";
        } else if (30 * 1024 > desFileSize) {
            return "_secondary";
        } else if (500 * 1024 > desFileSize) {
            return "_normal";
        } else {
            return "_big";
        }
    }
}
