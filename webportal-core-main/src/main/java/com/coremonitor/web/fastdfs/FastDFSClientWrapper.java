package com.coremonitor.web.fastdfs;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.exception.FdfsUnsupportStorePathException;
import com.github.tobato.fastdfs.proto.storage.DownloadByteArray;
import com.github.tobato.fastdfs.proto.storage.DownloadCallback;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by chenxuwu on 2018/1/2.
 */
@Component
//@PropertySource("common.properties")
public class FastDFSClientWrapper {
    private final Logger logger = LoggerFactory.getLogger(FastDFSClientWrapper.class);

    @Autowired
    private FastFileStorageClient storageClient;

    @Value("${fdfs.access.url}")
    private String address;

    /**
     * 上传multipartfile文件
     *
     * @param file 文件对象
     * @return 文件访问地址
     * @throws IOException
     */
    public String uploadFile(MultipartFile file) throws IOException {
        StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
        return storePath.getFullPath();
    }


    /**
     * 上传file文件
     *
     * @param file 文件对象
     * @return 文件访问地址
     * @throws IOException
     */
    public String uploadFile(File file) throws IOException {
        StorePath storePath = storageClient.uploadFile(new FileInputStream(file), file.length(), file.getName().substring(file.getName().lastIndexOf(".") + 1), null);
        return storePath.getFullPath();
    }

    /**
     * 将一段字符串生成一个文件上传
     *
     * @param content       文件内容
     * @param fileExtension
     * @return
     */
    public String uploadFile(String content, String fileExtension) {
        byte[] buff = content.getBytes(Charset.forName("UTF-8"));
        ByteArrayInputStream stream = new ByteArrayInputStream(buff);
        StorePath storePath = storageClient.uploadFile(stream, buff.length, fileExtension, null);
        return storePath.getFullPath();
    }


    /**
     * 删除文件
     *
     * @param fileUrl 文件访问地址
     * @return
     */
    public void deleteFile(String fileUrl) {
        if (StringUtils.isEmpty(fileUrl)) {
            return;
        }
        try {
            StorePath storePath = StorePath.praseFromUrl(fileUrl);
            storageClient.deleteFile(storePath.getGroup(), storePath.getPath());
        } catch (FdfsUnsupportStorePathException e) {
            logger.warn(e.getMessage());
        }
    }

    /**
     * 文件下载
     *
     * @return
     */
    public byte[] downloadFile(String uri) {
        DownloadCallback<byte[]> callback = new DownloadByteArray();
        String dir = uri.substring(0, uri.indexOf('/'));
        String file = uri.substring(uri.indexOf('/')+1);
        byte[] b = storageClient.downloadFile(dir, file, callback);
        return b;
    }

    /**
     * 拼接图片的完整路径
     *
     * @param url
     * @return
     */
    public String getFullUrl(String url) {
        String fileUrl = "http://" + address + "/" + url;
        return fileUrl;
    }

    /**
     * 文件下载方式2
     *
     * @param path
     * @param output
     * @return
     */
/*
    public int download_file(String path, BufferedOutputStream output) {
        //byte[] b = storageClient.download_file(group, path);
        int result = -1;
        DownloadCallback<byte[]> callback = new DownloadByteArray();
        try {
            byte[] b = (byte[]) storageClient.downloadFile("group1", "M00/00/02/CgrUxFp6fDCAaeX_AAH_FMOzqs4755.png", callback);
            try {
                if (b != null) {
                    output.write(b);
                    result = 0;
                }
            } catch (Exception e) {
            } //用户可能取消了下载
            finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
*/


}
