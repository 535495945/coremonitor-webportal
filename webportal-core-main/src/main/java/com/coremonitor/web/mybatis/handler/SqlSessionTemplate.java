package com.coremonitor.web.mybatis.handler;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Description sqlSession非线程安全，使用后需关闭释放到连接池，参考spring集成SqlSessionTemplate，
 * 使用动态代理对mapper接口类方法进行拦截，实现对sqlsession的使用和关闭释放处理，
 * 另外，类似spring事务对mapper操作的控制暂未实现，后续考虑是否需要引入(可以使用MybatisClient方式实现)
 * @Author liujiongxin
 * @Date 2019/1/25 17:37
 * @Version 1.0
 **/
public class SqlSessionTemplate implements InvocationHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SqlSessionTemplate.class);

    private Class mapperClazz;
    private SqlSessionFactory factory;
    private ExecutorType executorType;

    private SqlSessionTemplate() {
    }

    /**
     * 获取代理对象
     * @param clazz
     * @param factory
     * @param executorType
     * @return
     */
    public static Object getInstance(Class clazz, SqlSessionFactory factory, ExecutorType executorType) {
        SqlSessionTemplate proxy = new SqlSessionTemplate();
        proxy.mapperClazz = clazz;
        proxy.factory = factory;
        proxy.executorType = executorType;
        return Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, proxy);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LOGGER.info("SqlSessionProxy method: "+method.getName());
        //设置自动提交（效率会更高）
        SqlSession sqlSession = factory.openSession(executorType, true);
        try{
            Object result = method.invoke(sqlSession.getMapper(mapperClazz), args);
            //保证提交commit一下
            sqlSession.commit(true);
            return result;
        }catch (Exception ex){
            LOGGER.error("SQL EXECUT ERROR.", ex);
            throw ex;
        }finally {
            //关闭sqlsession,释放回pool
            sqlSession.close();
        }
    }
}
