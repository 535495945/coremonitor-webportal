package com.coremonitor.web.mybatis.shardingjdbc.rule;

import com.coremonitor.web.mybatis.shardingjdbc.IRuleConfig;
import io.shardingjdbc.core.api.config.TableRuleConfiguration;

/**
 * @Description 定义表sharding规则配置类
 * @Author liujiongxin
 * @Date 2019/2/25 18:30
 * @Version 1.0
 **/
public class TableShardingRuleConfig implements IRuleConfig {

    @Override
    public TableRuleConfiguration tableRuleConfig() {
        TableRuleConfiguration tableRuleConfig = new TableRuleConfiguration();
//        tableRuleConfig.setLogicTable("t_order");
//        tableRuleConfig.setActualDataNodes("ds_${0..1}.t_order_${[0, 1]}");
//
//        tableRuleConfig.setDatabaseShardingStrategyConfig(new InlineShardingStrategyConfiguration("user_id", "ds_${user_id % 2}"));
//        tableRuleConfig.setTableShardingStrategyConfig(new InlineShardingStrategyConfiguration("order_id", "t_order_${order_id % 2}"));
//
        return tableRuleConfig;
    }
}
