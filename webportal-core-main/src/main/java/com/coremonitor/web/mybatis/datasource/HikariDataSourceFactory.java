package com.coremonitor.web.mybatis.datasource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.datasource.DataSourceFactory;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Hikari 数据库连接池
 */
public class HikariDataSourceFactory implements DataSourceFactory {

    private Properties props;

    @Override
    public void setProperties(Properties props) {
        this.props = props;
    }

    @Override
    public DataSource getDataSource() {
        if(props == null){
            throw new IllegalArgumentException("DB properties is null.");
        }
        try{
            //适配原jdbc配置参数
            props.setProperty("driverClassName", this.props.getProperty("driverClass"));
            props.remove("driverClass");
            props.setProperty("username", this.props.getProperty("user"));
            props.remove("user");
            props.setProperty("connectionTestQuery","select 1");
            HikariConfig config = new HikariConfig(props);
//            config.setDriverClassName(this.props.getProperty("driverClass"));
//            config.setJdbcUrl(this.props.getProperty("jdbcUrl"));
//            config.setUsername(this.props.getProperty("user"));
//            config.setPassword(this.props.getProperty("password"));
//            config.setConnectionTimeout(Long.parseLong(String.valueOf(this.props.getOrDefault("connectionTimeout", "30000"))));
//            config.setConnectionTestQuery(String.valueOf(this.props.getOrDefault("connectionTestQuery","select 1")));
//            config.setIdleTimeout(Long.parseLong(String.valueOf(this.props.getOrDefault("idleTimeout","600000"))));
//            config.setMaxLifetime(Long.parseLong(String.valueOf(this.props.getOrDefault("maxLifetime","1800000"))));
//            config.setMinimumIdle(Integer.parseInt(String.valueOf(this.props.getOrDefault("minimumIdle","2"))));
//            config.setMaximumPoolSize(Integer.parseInt(String.valueOf(this.props.getOrDefault("maximumPoolSize","8"))));
//            config.setValidationTimeout(Long.parseLong(String.valueOf(this.props.getOrDefault("validationTimeout","5000"))));
//            config.setLeakDetectionThreshold(Long.parseLong(String.valueOf(this.props.getOrDefault("leakDetectionThreshold","0"))));
            HikariDataSource ds = new HikariDataSource(config);
            return ds;
        }catch (Exception ex){
            throw ex;
        }
    }
}
