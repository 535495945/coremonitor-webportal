package com.coremonitor.web.mybatis.shardingjdbc;

import io.shardingjdbc.core.api.config.TableRuleConfiguration;

/**
 * @ClassName IRuleConfig
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/14 18:11
 * @Version 1.0
 **/
public interface IRuleConfig {
    TableRuleConfiguration tableRuleConfig();
}
