package com.coremonitor.common.response;

import com.alibaba.fastjson.JSON;

/**
 * Created by chenxuwu on 2018/3/20.
 */
public class JsonResponse<T> {

    //状态
    private int status;

    //提示信息
    private String reason;

    public T data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public JsonResponse() {
    }

    public JsonResponse(int status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    public JsonResponse(int status, String reason, T data) {
        this.status = status;
        this.reason = reason;
        this.data = data;
    }

    public String toJson(){
        return JSON.toJSONString(this);
    }
}
