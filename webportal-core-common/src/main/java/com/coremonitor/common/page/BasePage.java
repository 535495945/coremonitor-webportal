package com.coremonitor.common.page;

public class BasePage<T> {
    //数据
    private T data;
    //总条数
    private int total;
    //总页数
    private int pageCount;
    //当前页数
    private int currentPage;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public BasePage(){};

    public BasePage(T data, int total, int currentPage, int pageCount) {
        this.data = data;
        this.total = total;
        this.currentPage = currentPage;
        this.pageCount = pageCount;
    }
}
