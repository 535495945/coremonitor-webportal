package com.coremonitor.common.vo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @ClassName RoleVo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/6 18:19
 * @Version 1.0
 **/
@ApiModel(value = "RoleVo", description = "用户角色对象")
public class RoleVo   implements Serializable{
    private Long id;
    private String name;
    private String url;

    public RoleVo() {
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleVo(Long id, String name) {

        this.id = id;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
