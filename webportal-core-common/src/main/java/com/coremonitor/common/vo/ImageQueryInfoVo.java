package com.coremonitor.common.vo;

import java.io.Serializable;

/**
 * @ClassName ImageQueryInfoVo
 * @Description 图像数据
 * @Author ROS
 * @Date 2019/4/6 20:16
 * @Version 1.0
 **/
public class ImageQueryInfoVo implements Serializable {
    /**
     * 当前碳块编号
     */
    private String carbonBlockNum;
    /**
     * 创建碳块时间
     */
    private String createTime;
    /**
     * 碳块质检状态0正常 1缺陷
     */
    private Integer status;
    /**
     * 标记状态 0未标记 1已标记
     */
    private Integer signStatus;
    /**
     * 缺陷详情
     */
    private String defectDetails;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 已读未读状态0未读 1已读
     */
    private Integer readState;
    /**
     * 开始位置
     */
    private Integer startIndex;
    /**
     * 当前展示数
     */
    private Integer displayIndex;
    /**
     * 总数
     */
    private Integer total;

    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public String getDefectDetails() {
        return defectDetails;
    }

    public void setDefectDetails(String defectDetails) {
        this.defectDetails = defectDetails;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getReadState() {
        return readState;
    }

    public void setReadState(Integer readState) {
        this.readState = readState;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(Integer displayIndex) {
        this.displayIndex = displayIndex;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
