package com.coremonitor.common.vo;

/**
 * @ClassName TokenVo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/13 11:21
 * @Version 1.0
 **/
public class TokenVo {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
