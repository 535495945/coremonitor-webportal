package com.coremonitor.common.vo;

import java.io.Serializable;

/**
 * @ClassName ReportFormInfoVo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/6 20:32
 * @Version 1.0
 **/
public class ReportFormInfoVo implements Serializable {
    /**
     * 缺陷类型
     */
    private Integer defectState;
    /**
     * 查询类型
     */
    private Integer checkType;
    /**
     * 展示时间
     */
    private String time;
    /**
     * 碳块总数
     */
    private Integer blockTotal;
    /**
     * 正常碳块总数
     */
    private Integer normalNum;
    /**
     * 缺陷总数
     */
    private Integer defectNum;
    /**
     * 良率
     */
    private String yield;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 开始位置
     */
    private String startIndex;
    /**
     * 当前展示数
     */
    private Integer displayIndex;
    /**
     * 总数
     */
    private String total;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getYield() {
        return yield;
    }

    public void setYield(String yield) {
        this.yield = yield;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }


    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Integer getDefectState() {
        return defectState;
    }

    public void setDefectState(Integer defectState) {
        this.defectState = defectState;
    }

    public Integer getCheckType() {
        return checkType;
    }

    public void setCheckType(Integer checkType) {
        this.checkType = checkType;
    }

    public Integer getBlockTotal() {
        return blockTotal;
    }

    public void setBlockTotal(Integer blockTotal) {
        this.blockTotal = blockTotal;
    }

    public Integer getNormalNum() {
        return normalNum;
    }

    public void setNormalNum(Integer normalNum) {
        this.normalNum = normalNum;
    }

    public Integer getDefectNum() {
        return defectNum;
    }

    public void setDefectNum(Integer defectNum) {
        this.defectNum = defectNum;
    }

    public Integer getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(Integer displayIndex) {
        this.displayIndex = displayIndex;
    }
}
