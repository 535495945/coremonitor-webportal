package com.coremonitor.common.vo;

/**
 * @ClassName BaseVo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/13 18:43
 * @Version 1.0
 **/
public class BaseVo {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
