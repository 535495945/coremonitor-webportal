package com.coremonitor.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName CarbonBlockDetailsVo
 * @Description 碳块详情
 * @Author ROS
 * @Date 2019/4/6 20:25
 * @Version 1.0
 **/
public class CarbonBlockDetailsVo  implements Serializable {
    /**
     * 生产线编号
     */
    private String lineNum;
    /**
     * 当前碳块编号
     */
    private String carbonBlockNum;
    /**
     * 创建碳块时间
     */
    private String createTime;
    /**
     * 碳块质检状态0正常 1缺陷
     */
    private Integer status;
    /**
     * 标记状态 0未标记 1已标记
     */
    private Integer signStatus;
    /**
     * 缺陷详情
     */
    private String defectDetails;

    private List<PhotoInfoVo> photoInfoVoList;

    public String getLineNum() {
        return lineNum;
    }

    public void setLineNum(String lineNum) {
        this.lineNum = lineNum;
    }

    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public String getDefectDetails() {
        return defectDetails;
    }

    public void setDefectDetails(String defectDetails) {
        this.defectDetails = defectDetails;
    }

    public List<PhotoInfoVo> getPhotoInfoVoList() {
        return photoInfoVoList;
    }

    public void setPhotoInfoVoList(List<PhotoInfoVo> photoInfoVoList) {
        this.photoInfoVoList = photoInfoVoList;
    }
}
