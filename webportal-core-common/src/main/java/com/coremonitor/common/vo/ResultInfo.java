package com.coremonitor.common.vo;

import io.swagger.models.auth.In;

/**
 * @ClassName ResultInfo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/17 23:08
 * @Version 1.0
 **/
public class ResultInfo {
    private Integer Ax;
    private Integer Ay;
    private Integer Bx;
    private Integer By;
    private Integer Cx;
    private Integer Cy;
    private Integer Dx;
    private Integer Dy;
    private Integer x;
    private Integer y;

    public Integer getAx() {
        return Ax;
    }

    public void setAx(Integer ax) {
        Ax = ax;
    }

    public Integer getAy() {
        return Ay;
    }

    public void setAy(Integer ay) {
        Ay = ay;
    }

    public Integer getBx() {
        return Bx;
    }

    public void setBx(Integer bx) {
        Bx = bx;
    }

    public Integer getBy() {
        return By;
    }

    public void setBy(Integer by) {
        By = by;
    }

    public Integer getCx() {
        return Cx;
    }

    public void setCx(Integer cx) {
        Cx = cx;
    }

    public Integer getCy() {
        return Cy;
    }

    public void setCy(Integer cy) {
        Cy = cy;
    }

    public Integer getDx() {
        return Dx;
    }

    public void setDx(Integer dx) {
        Dx = dx;
    }

    public Integer getDy() {
        return Dy;
    }

    public void setDy(Integer dy) {
        Dy = dy;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
