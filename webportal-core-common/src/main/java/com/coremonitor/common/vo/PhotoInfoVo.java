package com.coremonitor.common.vo;

import java.io.Serializable;

/**
 * @ClassName PhotoInfoVo
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/6 20:47
 * @Version 1.0
 **/
public class PhotoInfoVo implements Serializable {
    /**
     * 照片路径
     */
    private String photoPath;
    /**
     * 照片名称
     */
    private String photoName;
    /**
     * 照片状态
     */
    private Integer photoState;
    /**
     * 缺陷坐标
     */
    private String  resultInfo;

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public Integer getPhotoState() {
        return photoState;
    }

    public void setPhotoState(Integer photoState) {
        this.photoState = photoState;
    }

    public String getResultInfo() {
        return resultInfo;
    }

    public void setResultInfo(String resultInfo) {
        this.resultInfo = resultInfo;
    }
}
