package com.coremonitor.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName MonitorInfoVo
 * @Description 实时碳块数据监控
 * @Author ROS
 * @Date 2019/4/6 20:06
 * @Version 1.0
 **/
public class MonitorInfoVo implements Serializable {
    /**
     * 缺陷碳块数量
     */
    private Integer defectNum;

    /**
     * 生产线编号
     */
    private String lineNum;

    /**
     * 当天生产碳块数量
     */
    private Integer theDayNum;

    /**
     * 当前碳块编号
     */
    private String carbonBlockNum;

    /**
     * 创建碳块时间
     */
    private String createTime;

    /**
     * 当前已经识别照片数
     */
    private Integer carbonBlockHandle;

    /**
     * 碳块质检状态0正常 1缺陷
     */
    private Integer status;

    /**
     * 标记状态 0未标记 1已标记
     */
    private Integer signStatus;

    /**
     * 缺陷详情
     */
    private String defectDetails;

    /**
     * 照片内容
     */
    private List<PhotoInfoVo> photoInfoVo;


    public String getLineNum() {
        return lineNum;
    }

    public void setLineNum(String lineNum) {
        this.lineNum = lineNum;
    }


    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public String getDefectDetails() {
        return defectDetails;
    }

    public void setDefectDetails(String defectDetails) {
        this.defectDetails = defectDetails;
    }

    public List<PhotoInfoVo> getPhotoInfoVo() {
        return photoInfoVo;
    }

    public void setPhotoInfoVo(List<PhotoInfoVo> photoInfoVo) {
        this.photoInfoVo = photoInfoVo;
    }

    public Integer getDefectNum() {
        return defectNum;
    }

    public void setDefectNum(Integer defectNum) {
        this.defectNum = defectNum;
    }

    public Integer getTheDayNum() {
        return theDayNum;
    }

    public void setTheDayNum(Integer theDayNum) {
        this.theDayNum = theDayNum;
    }

    public Integer getCarbonBlockHandle() {
        return carbonBlockHandle;
    }

    public void setCarbonBlockHandle(Integer carbonBlockHandle) {
        this.carbonBlockHandle = carbonBlockHandle;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }
}
