package com.coremonitor.common.request;

import com.coremonitor.common.vo.BaseVo;

/**
 * @ClassName SignBlockReq
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/14 12:17
 * @Version 1.0
 **/
public class SignBlockReq extends BaseVo{
    /**
     * 碳快编号
     */
    private String carbonBlockNum;

    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }
}
