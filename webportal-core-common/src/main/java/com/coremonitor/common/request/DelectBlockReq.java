package com.coremonitor.common.request;

import com.coremonitor.common.vo.BaseVo;

/**
 * @ClassName DelectBlockReq
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/14 12:16
 * @Version 1.0
 **/
public class DelectBlockReq extends BaseVo {
    /**
     * 碳快编号
     */
    private String carbonBlockNum;

    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }
}
