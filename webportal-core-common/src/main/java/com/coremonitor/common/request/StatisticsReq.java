package com.coremonitor.common.request;

import com.coremonitor.common.vo.BaseVo;
import io.swagger.models.auth.In;

/**
 * @ClassName StatisticsReq
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/14 12:27
 * @Version 1.0
 **/
public class StatisticsReq extends BaseVo {
    private Integer defectState;
    private Integer checkType;

    public Integer getDefectState() {
        return defectState;
    }

    public void setDefectState(Integer defectState) {
        this.defectState = defectState;
    }
}
