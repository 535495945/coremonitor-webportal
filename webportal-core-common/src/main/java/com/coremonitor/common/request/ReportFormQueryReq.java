package com.coremonitor.common.request;

import com.coremonitor.common.vo.BaseVo;

/**
 * @ClassName ReportFormQueryReq
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/14 12:19
 * @Version 1.0
 **/
public class ReportFormQueryReq extends BaseVo {
    /**
     * 缺陷类型
     */
    private Integer defectState;
    /**
     * 查询类型
     */
    private Integer checkType;
    private String startTime;
    private String endTime;

    public Integer getDefectState() {
        return defectState;
    }

    public void setDefectState(Integer defectState) {
        this.defectState = defectState;
    }

    public Integer getCheckType() {
        return checkType;
    }

    public void setCheckType(Integer checkType) {
        this.checkType = checkType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
