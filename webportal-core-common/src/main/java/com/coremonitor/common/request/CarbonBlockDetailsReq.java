package com.coremonitor.common.request;

import com.coremonitor.common.vo.BaseVo;

/**
 * @ClassName CarbonBlockDetailsReq
 * @Description TODO
 * @Author ROS
 * @Date 2019/4/14 12:07
 * @Version 1.0
 **/
public class CarbonBlockDetailsReq extends BaseVo {
    /**
     * 碳快编号
     */
    private String carbonBlockNum;

    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }
}
