package com.coremonitor.common.request;

import com.coremonitor.common.vo.BaseVo;

import java.util.Date;

/**
 * @ClassName ImageQuereReq
 * @Description 图像查询
 * @Author ROS
 * @Date 2019/4/14 11:46
 * @Version 1.0
 **/
public class ImageQuereReq extends BaseVo{
    private Integer status;
    private Integer signStatus;
    private String carbonBlockNum;
    private Integer firstIndex;
    private Integer pageSize;
    private String startTime;
    private String endTime;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public String getCarbonBlockNum() {
        return carbonBlockNum;
    }

    public void setCarbonBlockNum(String carbonBlockNum) {
        this.carbonBlockNum = carbonBlockNum;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getFirstIndex() {
        return firstIndex;
    }

    public void setFirstIndex(Integer firstIndex) {
        this.firstIndex = firstIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
